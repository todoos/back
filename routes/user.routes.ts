const Router = require('express')
const router = new Router()
const userController = require('../controller/user.conroller')
import {check} from 'express-validator'

const authMiddleware = require("../middleware/authMiddleware")

router.post('/register', [
    check('name', 'Поле name не должно быть пустым').notEmpty(),
    check('name', "Поле name должно быть длинной от 4 и до 15 символов").isLength({
        min: 4,
        max: 15,
    }),
    check('password', 'Поле password не должно быть пустым').notEmpty(),
    check('password', "Поле password должно быть длинной от 4 и до 15 символов").isLength({
        min: 4,
        max: 15,
    })
], userController.register)

router.post('/login', [
    check('login', 'Поле login не должно быть пустым').notEmpty(),
    check('login', "Поле login должно быть длинной от 4 и до 15 символов").isLength({
        min: 4,
        max: 15,
    }),
    check('password', 'Поле password не должно быть пустым').notEmpty(),
    check('password', "Поле password должно быть длинной от 4 и до 15 символов").isLength({
        min: 4,
        max: 15,
    })
], userController.login)


router.get('/users/me', authMiddleware, userController.getCurrentUser)
router.get('/users', authMiddleware, userController.getUsers)
router.post('/users/update', authMiddleware, userController.updateUser)

module.exports = router