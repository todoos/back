const todosController = require('../controller/todos.controller')
import {check} from 'express-validator'

const Router = require('express')
const router = new Router()
const authMiddleware = require("../middleware/authMiddleware")

enum Priority {
    high = 'high',
    middle = 'middle',
    low = 'low',
}

router.post('/create', [
    check('title', 'title name не должно быть пустым').notEmpty(),
    check('title', "Поле title должно быть длинной" +
        " от 4 и до 15 символов").isLength({
        min: 4,
        max: 30,
    }),
    check('priority', 'Поле priority не должно быть пустым').notEmpty(),
    check('priority', `Некорректное значение priority, должно быть одно из: ${Object.values(Priority)}`).isIn(Object.values(Priority)),
    check('description', 'Поле description не должно быть пустым').notEmpty(),
    check('assignee_id', 'Поле assignee_id не должно быть пустым').notEmpty(),
    check('sub_todos', 'Поле sub_todos не должно быть пустым').notEmpty(),
], authMiddleware, todosController.createTodos)

router.get('/my', authMiddleware, todosController.getMyTasks)
router.get('/delegated', authMiddleware, todosController.getDelegatedTasks)
router.post('/delete', authMiddleware, todosController.deleteTodos)

module.exports = router