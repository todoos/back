FROM node:18.20-alpine3.19

WORKDIR /app
COPY . .
RUN npm ci
EXPOSE 8001
CMD [ "npm", "start"]