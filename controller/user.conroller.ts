import {Request, Response} from 'express'
import {RequestNew} from "../middleware/authMiddleware";
import {validationResult} from 'express-validator'

const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const db = require('../pool/db')
const secret = require('../config')

const generateAccessToken = (id: string) => {
    const payload = {
        id: id,
    }
    const {code} = secret
    return jwt.sign(payload, code, {expiresIn: '98h'})
}

class UserController {
    async register(req: Request, res: Response) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'Ошибка при регистрации', errors})
            }
            const {name, password} = req.body
            const hashPassword = bcrypt.hashSync(password.trim(), 7)
            const newPerson = await db.query(`INSERT INTO users (name, password) values ($1, $2) RETURNING *`, [name.trim(), hashPassword.trim()])
            const curID = newPerson.rows[0].id

            const token = generateAccessToken(String(curID))
            return res.json({token: `${token}`})

        } catch (e) {
            console.error(e)
            res.status(400).json({message: `${e}`})
        }
    }

    async login(req: Request, res: Response) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'Ошибка при авторизации', errors})
            }
            const {login, password} = req.body
            const user = await db.query(`SELECT * FROM users where name = $1`, [login.trim()])
            if (!user.rows.length) {
                return res.status(400).json({message: `пользователь не найден`})
            }
            const isValidAuth = bcrypt.compareSync(password.trim(), user.rows[0].password.trim())
            if (isValidAuth) {
                const token = generateAccessToken(String(user.rows[0].id))
                return res.json({token: `${token}`})
            }
            return res.status(400).json({message: `неверный пароль`})
        } catch (e) {
            console.error(e)
            res.status(400).json({message: `${e}`})
        }
    }

    async getUsers(req: RequestNew, res: Response) {
        const users = await db.query(`SELECT id, name, avatar, team, status FROM users`)
        return res.json(users.rows)
    }

    async getCurrentUser(req: RequestNew, res: Response) {
        const users = await db.query(`SELECT id, name, avatar, team, status FROM users WHERE id = ${req.user.id}`)
        if (!users.rows.length) {
            return res.status(401).json({message: `Пользователь не найден`})
        }
        return res.json(users.rows[0])
    }

    async updateUser(req: RequestNew, res: Response) {
        try {
            const {avatar, team, status} = req.body
            if (!avatar && !team && !status) {
                return res.status(400).json({message: 'Необходимо передать хотя бы одно поле: avatar, team, status'});
            }
            const dbUsers = await db.query(`SELECT * FROM users WHERE id = ${req.user.id}`)
            if (!dbUsers?.rows.length) {
                return res.status(401).json({message: `Пользователь не найден`})
            }
            const dbUser = dbUsers.rows[0]
            await db.query(`
                    UPDATE users
                    SET avatar = $1, team = $2, status = $3
                    WHERE id = $4
                `, [avatar || dbUser?.avatar, team || dbUser?.team, status || dbUser?.status, req?.user.id])
            return res.json({message: `success`})
        } catch (e) {
            console.error(e)
            res.status(400).json({message: `error`})
        }
    }
}

module.exports = new UserController()