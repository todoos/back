import {Response} from 'express'
import {validationResult} from 'express-validator'
import {RequestNew} from "../middleware/authMiddleware";

const db = require('../pool/db')

enum Status {
    inProgress = 'inProgress',
    completed = 'completed',
    declined = 'declined',
}

class todosController {
    async createTodos(req: RequestNew, res: Response) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'ошибка при создание todo', errors})
            }
            const {description, title, priority, assignee_id, sub_todos} = req.body
            const user = await db.query(`SELECT * FROM users where id = $1`, [Number(req?.user.id)])
            if (!user.rows.length) {
                return res.status(400).json({message: `нельзя добавить todo для не существующего пользователя`})
            }
            //TODO добавить слой обработки ошибок
            const newTodos = await db.query(`INSERT INTO todos ("author_id", "title","priority","assignee_id", "description", "status") values ($1, $2, $3, $4, $5, $6) RETURNING id`,
                [Number(req.user.id), title.trim(), priority.trim(), assignee_id, description.trim(), Status.inProgress]
            )
            const newTodoId = newTodos.rows[0].id
            if (!newTodoId) {
                return res.status(400).json({message: `не удалось создать тудус`})
            }
            if (sub_todos.length) {
                const values = sub_todos.map((todos: string, index: number) => `($1, 'inProgress', $${index + 2})`).join(', ');
                const params = [Number(newTodoId), ...sub_todos];
                const query = `INSERT INTO sub_todos ("todos_id", "status", "description") VALUES ${values} RETURNING *`;
                await db.query(query, params);
            }
            return res.status(201).json({id: newTodoId})
        } catch (e) {
            console.error(e)
            return res.status(400).json({message: `${e}`})
        }
    }


    async deleteTodos(req: RequestNew, res: Response) {
        try {
            const {id} = req.body
            const dbResult = await db.query(`SELECT * FROM todos WHERE id = ${id}`)
            if (!dbResult.rows.length) {
                return res.status(400).json({message: `todos с id=${id} не найден`})
            }
            const dbTodos = dbResult.rows[0]
            if (dbTodos.author_id !== Number(req.user.id)) {
                return res.status(403).json({message: `Пользователь с id=${req.user.id} не может удалить todos пользователя ${dbTodos.author_id}`})
            }
            await db.query(`DELETE FROM sub_todos WHERE todos_id = ${id}`)
            await db.query(`DELETE FROM todos WHERE id = ${id}`)
            return res.status(204).json()
        } catch (e) {
            console.error(e)
            return res.status(400).json({message: `${e}`})
        }
    }

    async getMyTasks(req: RequestNew, res: Response) {
        const result = await db.query(`
          SELECT
                t.id,
                t.priority,
                t.author_id,
                t.assignee_id,
                t.status,
                t.title,
                t.description,
                t.created_at,
                COALESCE(
                  json_agg(
                    json_build_object(
                      'id', st.id,
                      'status', st.status,
                      'description', st.description
                    )
                  ) FILTER (WHERE st.id IS NOT NULL), '[]'
                ) AS sub_todos
          FROM todos t
          LEFT JOIN sub_todos st ON t.id = st.todos_id
          WHERE t.assignee_id = ${req.user.id}
          GROUP BY t.id
        `)
        return res.json(result.rows)
    }

    async getDelegatedTasks(req: RequestNew, res: Response) {
        const result = await db.query(`
          SELECT
                t.id,
                t.priority,
                t.author_id,
                t.assignee_id,
                t.status,
                t.title,
                t.description,
                t.created_at,
                COALESCE(
                  json_agg(
                    json_build_object(
                      'id', st.id,
                      'status', st.status,
                      'description', st.description
                    )
                  ) FILTER (WHERE st.id IS NOT NULL), '[]'
                ) AS sub_todos
          FROM todos t
          LEFT JOIN sub_todos st ON t.id = st.todos_id
          WHERE t.author_id = ${req.user.id} AND t.assignee_id != ${req.user.id}
          GROUP BY t.id
        `)
        return res.json(result.rows)
    }
}

module.exports = new todosController()