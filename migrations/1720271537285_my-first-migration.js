/**
 * @type {import('node-pg-migrate').ColumnDefinitions | undefined}
 */
exports.shorthands = undefined

/**
 * @param pgm {import('node-pg-migrate').MigrationBuilder}
 * @param run {() => void | undefined}
 * @returns {Promise<void> | void}
 */
exports.up = (pgm) => {
    pgm.createTable('users', {
        id: {type: 'serial', primaryKey: true,  },
        name: { type: 'varchar(1000)', notNull: true, unique: true },
        avatar: { type: 'varchar(1000)' },
        team: { type: 'varchar(1000)' },
        status: { type: 'varchar(1000)' },
        password: { type: 'varchar(100000)', notNull: true },
        created_at: {
            type: 'timestamp',
            notNull: true,
            default: pgm.func('current_timestamp'),
        },
    })
    pgm.createTable('todos', {
        id:  {type: 'serial', primaryKey: true},
        priority: {type: 'text', notNull: true},
        author_id: {
            type: 'integer',
            notNull: true,
            references: '"users"',
            onDelete: 'cascade',
        },
        assignee_id: {
            type: 'integer',
            notNull: true,
            onDelete: 'cascade',
            references: '"users"',
        },
        status: { type: 'text', notNull: true },
        title: { type: 'text', notNull: true },
        description: { type: 'text'},
        created_at: {
            type: 'timestamp',
            notNull: true,
            default: pgm.func('current_timestamp'),
        },
    })

    pgm.createTable('sub_todos', {
        id: {type: 'serial', primaryKey: true},
        status: { type: 'text', notNull: true },
        todos_id: {
            type: 'integer',
            notNull: true,
            references: '"todos"',
            onDelete: 'cascade',
        },
        description: { type: 'text', notNull: true },
    })
}

/**
 * @param pgm {import('node-pg-migrate').MigrationBuilder}
 * @param run {() => void | undefined}
 * @returns {Promise<void> | void}
 */
exports.down = (pgm) => {
    pgm.dropTable('sub_todos')
    pgm.dropTable('todos')
    pgm.dropTable('users')
}
