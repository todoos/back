const express = require('express');
require('dotenv').config()
const users = require('./routes/user.routes.ts')
const todos = require('./routes/todos.register')
const cors = require('cors')


const corsOptions = {
    origin: '*',
}

const PORT = process.env.PORT || 8081
const app = express()

app.use(cors(corsOptions))
app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.use('/api', users)
app.use('/todos', todos)

app.listen(PORT)