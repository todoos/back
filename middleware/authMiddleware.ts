import {Request, Response} from 'express'

const {code} = require('.././config')
const jwt = require('jsonwebtoken')

interface User {
    id: string;
    iat: number;
    exp: number;
}

export interface RequestNew extends Request {
    user: User;
}

module.exports = function (req: Request | any, res: Response, next: any) {
    if (req.method === "OPTIONS") {
        next()
    }
    try {
        const token = req.headers.authorization
        if (!token) {
            return res.status(403).json({message: 'Пользователь не авторизован'})
        }
        const decodeData = jwt.verify(token, code)
        req.user = decodeData
        if (!decodeData.id) {
            return res.status(403).json({message: 'Ошибка '})
        }
        next()
    } catch (e) {
        console.error(e)
        return res.status(403).json({message: 'Пользователь не авторизован'})
    }
}